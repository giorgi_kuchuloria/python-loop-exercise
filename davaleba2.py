# ფუნქცია პოულობს გამყოფებს და აჯამებს მათ
def divisors(j):
    sum = 0
    # i არის ყველა რიცხვი j-მდე. მაგ: 1,2,3,4,5 ა.შ.
    for i in range(1,j):
        #თუ j უნაშთოდ  იყოფა i-ზე ესეიგი გამყოფი ნაპოვნია
        if j % i == 0:
            # sum გამოიყენება გამყოფების შესაკრებად
           sum += i
    return sum

for i in range (1,10):
    # თუ i არის თავისი თავის ტოლი მაშინ დაბეჭდოს  მონაცემი
    if i ==  divisors(j = i):
        print(f'number:{i} and divisor sum:  {divisors(i)}')